package com.bespin.alertnow.alertprocessor.alertnowprocessor.controller;


import com.bespin.alertnow.alertprocessor.alertnowprocessor.service.MessageService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(value = "/mapping")
public class MapperController {
    @Autowired
    MessageService messageService;

    @RequestMapping(value = "/{apikey}/{integration-type}", method = RequestMethod.POST)
    public ResponseEntity<JSONObject> addOrUpdateIntegrationType(@PathVariable(name = "apikey") String apikey, @PathVariable(name = "integration-type") String integrationType, @RequestBody String message){
        JSONObject returnObject = messageService.standardizedMessage(apikey,integrationType, message);
        return ok(returnObject);
    }
}
