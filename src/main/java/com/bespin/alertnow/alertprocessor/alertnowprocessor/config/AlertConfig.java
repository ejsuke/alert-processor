package com.bespin.alertnow.alertprocessor.alertnowprocessor.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "alert")
public class AlertConfig {
    private String redisMap;
    private String defaultKey;

    public String getDefaultKey() {
        return defaultKey;
    }

    public void setDefaultKey(String defaultKey) {
        this.defaultKey = defaultKey;
    }

    public String getRedisMap() {
        return redisMap;
    }

    public void setRedisMap(String redisMap) {
        this.redisMap = redisMap;
    }
}
