package com.bespin.alertnow.alertprocessor.alertnowprocessor.service;

import com.bespin.alertnow.alertprocessor.alertnowprocessor.common.Utils;
import com.bespin.alertnow.alertprocessor.alertnowprocessor.modal.MappingConfig;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Iterator;
import java.util.Objects;

@Service
public class MessageService {
    @Autowired
    MappingConfigService mappingConfigService;

    private static final ObjectMapper mapper = Utils.MAPPER;

    public JSONObject standardizedMessage(String apiKey, String integrationType, String message) {
        MappingConfig mappingConfig = mappingConfigService.getApiMappingConfig(apiKey, integrationType);
        JSONObject jsonOutputMessage = new JSONObject();
        try {
            Thread.sleep(5000);
            System.out.println("process message:" +message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            JsonNode jsonConfig = mapper.readTree(mappingConfig.getConfig().replace("'", "\""));
            JsonNode jsonMessage = mapper.readTree(message);
            Iterator<String> fieldNames = jsonConfig.fieldNames();
            while (fieldNames.hasNext()) {
                String itemName = fieldNames.next();
                if("getAll".equals(itemName)){
                    JSONParser parser = new JSONParser();
                    jsonOutputMessage = (JSONObject) parser.parse(message);
                    break;
                }
                JsonNode item = jsonConfig.get(itemName);
                if (item.size() > 1) {
                    throw new Exception("Config item not valid");
                }
                // get value
                String defaultValue = item.toString().split("\\|\\|").length > 1 ? item.textValue().split("\\|\\|")[1] : "";

                String[] fieldLocation = item.toString().split("\\|\\|")[0].replace("\"","").split("\\.");
                String propertiesValue = getDeepJson(fieldLocation.length - 1, jsonMessage, fieldLocation, 0, defaultValue.replace("\"",""));

                //create json node
                String[] fieldNameLocation = itemName.split("\\.");
                jsonOutputMessage = createDeepJson(fieldNameLocation.length - 1, jsonOutputMessage, fieldNameLocation, 0, propertiesValue);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonOutputMessage;
    }

    public JSONObject createDeepJson(int level, JSONObject jsonObject,  String[] itemNames, int count, String value){
        if(level > 0){
            JSONObject childJson = new JSONObject();
            if(Objects.isNull(jsonObject.get(itemNames[count]))){
                jsonObject.put(itemNames[count],childJson);
            }else {
                childJson = (JSONObject) jsonObject.get(itemNames[count]);
            }
            createDeepJson(level - 1, childJson, itemNames, count +1, value);
        } else {
            jsonObject.put(itemNames[count],value);
        }
        return jsonObject;
    }



    public String getDeepJson(int level, JsonNode node, String[] fieldLocaiton, int count, String defaultValue) {
        if (level > 0) {
            return getDeepJson(level - 1, node.get(fieldLocaiton[count]), fieldLocaiton, count + 1, defaultValue);
        } else {
            if(Objects.isNull(node.get(fieldLocaiton[count]))){
                return defaultValue;
            }
            return node.get(fieldLocaiton[count]).asText(defaultValue).replace("\"", "");
        }
    }
}
