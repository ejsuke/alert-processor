package com.bespin.alertnow.alertprocessor.alertnowprocessor.service;

import com.bespin.alertnow.alertprocessor.alertnowprocessor.config.AlertConfig;
import com.bespin.alertnow.alertprocessor.alertnowprocessor.modal.ApikeyMappingConfig;
import com.bespin.alertnow.alertprocessor.alertnowprocessor.modal.MappingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
public class MappingConfigService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private AlertConfig alertConfig;

    public MappingConfig getApiMappingConfig(String apikey, String integrationType){
        com.bespin.alertnowingestion.alertnowingestion.modal.ApikeyMappingConfig existedApikeyMappingConfig = (com.bespin.alertnowingestion.alertnowingestion.modal.ApikeyMappingConfig) redisTemplate.opsForHash().get(alertConfig.getRedisMap(), apikey);
        if (Objects.isNull(existedApikeyMappingConfig)) {
            existedApikeyMappingConfig = (com.bespin.alertnowingestion.alertnowingestion.modal.ApikeyMappingConfig) redisTemplate.opsForHash().get(alertConfig.getRedisMap(), alertConfig.getDefaultKey());
        }
        ApikeyMappingConfig converted = convert(existedApikeyMappingConfig);
        return converted.getMappingConfigs().get(integrationType);
    }
    public ApikeyMappingConfig convert(com.bespin.alertnowingestion.alertnowingestion.modal.ApikeyMappingConfig existedApikeyMappingConfig){
        ApikeyMappingConfig processor = new ApikeyMappingConfig();
        processor.setApiKey(existedApikeyMappingConfig.getApiKey());
        HashMap<String, MappingConfig> mappingConfigs = new HashMap<>();
        existedApikeyMappingConfig.getMappingConfigs().forEach((item, item2) -> {
            MappingConfig mappingConfig = new MappingConfig();
            mappingConfig.setConfig(item2.getConfig());
            mappingConfig.setIntegrationType(item2.getIntegrationType());
            mappingConfigs.put(item,mappingConfig);
        });
        processor.setMappingConfigs(mappingConfigs);
        processor.setApiKey(existedApikeyMappingConfig.getApiKey());
        return processor;
    }
}
