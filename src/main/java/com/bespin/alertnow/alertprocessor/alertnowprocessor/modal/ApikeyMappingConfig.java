package com.bespin.alertnow.alertprocessor.alertnowprocessor.modal;

import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

import java.io.Serializable;
import java.util.Map;

public class ApikeyMappingConfig extends JdkSerializationRedisSerializer implements Serializable  {
    private String apiKey;
    private Map<String, MappingConfig> mappingConfigs;

    public ApikeyMappingConfig(String apiKey, Map<String, MappingConfig> mappingConfigs) {
        this.apiKey = apiKey;
        this.mappingConfigs = mappingConfigs;
    }

    public ApikeyMappingConfig() {
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Map<String, MappingConfig> getMappingConfigs() {
        return mappingConfigs;
    }

    public void setMappingConfigs(Map<String, MappingConfig> mappingConfigs) {
        this.mappingConfigs = mappingConfigs;
    }
}
