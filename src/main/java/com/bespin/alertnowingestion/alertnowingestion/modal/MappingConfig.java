package com.bespin.alertnowingestion.alertnowingestion.modal;

import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;

import java.io.Serializable;

public class MappingConfig extends JdkSerializationRedisSerializer implements Serializable {
    String integrationType;
    String config;

    public MappingConfig(String integrationType, String config) {
        this.integrationType = integrationType;
        this.config = config;
    }

    public MappingConfig() {
    }

    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }
}
